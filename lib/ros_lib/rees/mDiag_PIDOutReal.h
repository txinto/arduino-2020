#ifndef _ROS_rees_mDiag_PIDOutReal_h
#define _ROS_rees_mDiag_PIDOutReal_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace rees
{

  class mDiag_PIDOutReal : public ros::Msg
  {
    public:
      typedef bool _diag_en_type;
      _diag_en_type diag_en;
      typedef uint16_t _diag_type;
      _diag_type diag;

    mDiag_PIDOutReal():
      diag_en(0),
      diag(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      *(outbuffer + offset + 0) = (this->diag >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->diag >> (8 * 1)) & 0xFF;
      offset += sizeof(this->diag);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
      this->diag =  ((uint16_t) (*(inbuffer + offset)));
      this->diag |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->diag);
     return offset;
    }

    const char * getType(){ return "rees/mDiag_PIDOutReal"; };
    const char * getMD5(){ return "f1ca14a1107401bac5cb9003483bec5f"; };

  };

}
#endif
