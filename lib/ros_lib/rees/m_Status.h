#ifndef _ROS_rees_m_Status_h
#define _ROS_rees_m_Status_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "rees/t_Status.h"

namespace rees
{

  class m_Status : public ros::Msg
  {
    public:
      typedef rees::t_Status _dre_type;
      _dre_type dre;
      typedef rees::t_Status _diag_type;
      _diag_type diag;
      typedef bool _diag_en_type;
      _diag_en_type diag_en;

    m_Status():
      dre(),
      diag(),
      diag_en(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      offset += this->dre.serialize(outbuffer + offset);
      offset += this->diag.serialize(outbuffer + offset);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      offset += this->dre.deserialize(inbuffer + offset);
      offset += this->diag.deserialize(inbuffer + offset);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
     return offset;
    }

    const char * getType(){ return "rees/m_Status"; };
    const char * getMD5(){ return "09ed6cff4c847742afb869795a617077"; };

  };

}
#endif
