#ifndef _ROS_rees_m_FrequencySetpointHysteresisLOW_h
#define _ROS_rees_m_FrequencySetpointHysteresisLOW_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace rees
{

  class m_FrequencySetpointHysteresisLOW : public ros::Msg
  {
    public:
      typedef uint16_t _dre_type;
      _dre_type dre;
      typedef uint16_t _diag_type;
      _diag_type diag;
      typedef bool _diag_en_type;
      _diag_en_type diag_en;

    m_FrequencySetpointHysteresisLOW():
      dre(0),
      diag(0),
      diag_en(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->dre >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->dre >> (8 * 1)) & 0xFF;
      offset += sizeof(this->dre);
      *(outbuffer + offset + 0) = (this->diag >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->diag >> (8 * 1)) & 0xFF;
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->dre =  ((uint16_t) (*(inbuffer + offset)));
      this->dre |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->dre);
      this->diag =  ((uint16_t) (*(inbuffer + offset)));
      this->diag |= ((uint16_t) (*(inbuffer + offset + 1))) << (8 * 1);
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
     return offset;
    }

    const char * getType(){ return "rees/m_FrequencySetpointHysteresisLOW"; };
    const char * getMD5(){ return "7bb75cca445c67ced45c482425497796"; };

  };

}
#endif
