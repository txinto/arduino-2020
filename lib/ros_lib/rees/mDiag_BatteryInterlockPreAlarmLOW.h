#ifndef _ROS_rees_mDiag_BatteryInterlockPreAlarmLOW_h
#define _ROS_rees_mDiag_BatteryInterlockPreAlarmLOW_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace rees
{

  class mDiag_BatteryInterlockPreAlarmLOW : public ros::Msg
  {
    public:
      typedef bool _diag_en_type;
      _diag_en_type diag_en;
      typedef bool _diag_type;
      _diag_type diag;

    mDiag_BatteryInterlockPreAlarmLOW():
      diag_en(0),
      diag(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      union {
        bool real;
        uint8_t base;
      } u_diag;
      u_diag.real = this->diag;
      *(outbuffer + offset + 0) = (u_diag.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
      union {
        bool real;
        uint8_t base;
      } u_diag;
      u_diag.base = 0;
      u_diag.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag = u_diag.real;
      offset += sizeof(this->diag);
     return offset;
    }

    const char * getType(){ return "rees/mDiag_BatteryInterlockPreAlarmLOW"; };
    const char * getMD5(){ return "a66cec0efddef40d11b9a7ce98d83891"; };

  };

}
#endif
