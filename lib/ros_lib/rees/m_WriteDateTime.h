#ifndef _ROS_rees_m_WriteDateTime_h
#define _ROS_rees_m_WriteDateTime_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace rees
{

  class m_WriteDateTime : public ros::Msg
  {
    public:
      typedef uint32_t _dre_type;
      _dre_type dre;
      typedef uint32_t _diag_type;
      _diag_type diag;
      typedef bool _diag_en_type;
      _diag_en_type diag_en;

    m_WriteDateTime():
      dre(0),
      diag(0),
      diag_en(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->dre >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->dre >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->dre >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->dre >> (8 * 3)) & 0xFF;
      offset += sizeof(this->dre);
      *(outbuffer + offset + 0) = (this->diag >> (8 * 0)) & 0xFF;
      *(outbuffer + offset + 1) = (this->diag >> (8 * 1)) & 0xFF;
      *(outbuffer + offset + 2) = (this->diag >> (8 * 2)) & 0xFF;
      *(outbuffer + offset + 3) = (this->diag >> (8 * 3)) & 0xFF;
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->dre =  ((uint32_t) (*(inbuffer + offset)));
      this->dre |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->dre |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->dre |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->dre);
      this->diag =  ((uint32_t) (*(inbuffer + offset)));
      this->diag |= ((uint32_t) (*(inbuffer + offset + 1))) << (8 * 1);
      this->diag |= ((uint32_t) (*(inbuffer + offset + 2))) << (8 * 2);
      this->diag |= ((uint32_t) (*(inbuffer + offset + 3))) << (8 * 3);
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
     return offset;
    }

    const char * getType(){ return "rees/m_WriteDateTime"; };
    const char * getMD5(){ return "9d9de2026927ec8a2549c8aa4f7caf57"; };

  };

}
#endif
