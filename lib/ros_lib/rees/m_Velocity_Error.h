#ifndef _ROS_rees_m_Velocity_Error_h
#define _ROS_rees_m_Velocity_Error_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace rees
{

  class m_Velocity_Error : public ros::Msg
  {
    public:
      typedef bool _dre_type;
      _dre_type dre;
      typedef bool _diag_type;
      _diag_type diag;
      typedef bool _diag_en_type;
      _diag_en_type diag_en;

    m_Velocity_Error():
      dre(0),
      diag(0),
      diag_en(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_dre;
      u_dre.real = this->dre;
      *(outbuffer + offset + 0) = (u_dre.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->dre);
      union {
        bool real;
        uint8_t base;
      } u_diag;
      u_diag.real = this->diag;
      *(outbuffer + offset + 0) = (u_diag.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_dre;
      u_dre.base = 0;
      u_dre.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->dre = u_dre.real;
      offset += sizeof(this->dre);
      union {
        bool real;
        uint8_t base;
      } u_diag;
      u_diag.base = 0;
      u_diag.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag = u_diag.real;
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
     return offset;
    }

    const char * getType(){ return "rees/m_Velocity_Error"; };
    const char * getMD5(){ return "e5a6133d923d4c62b94337157b5eec14"; };

  };

}
#endif
