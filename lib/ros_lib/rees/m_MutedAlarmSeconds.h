#ifndef _ROS_rees_m_MutedAlarmSeconds_h
#define _ROS_rees_m_MutedAlarmSeconds_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"

namespace rees
{

  class m_MutedAlarmSeconds : public ros::Msg
  {
    public:
      typedef uint8_t _dre_type;
      _dre_type dre;
      typedef uint8_t _diag_type;
      _diag_type diag;
      typedef bool _diag_en_type;
      _diag_en_type diag_en;

    m_MutedAlarmSeconds():
      dre(0),
      diag(0),
      diag_en(0)
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      *(outbuffer + offset + 0) = (this->dre >> (8 * 0)) & 0xFF;
      offset += sizeof(this->dre);
      *(outbuffer + offset + 0) = (this->diag >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      this->dre =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->dre);
      this->diag =  ((uint8_t) (*(inbuffer + offset)));
      offset += sizeof(this->diag);
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
     return offset;
    }

    const char * getType(){ return "rees/m_MutedAlarmSeconds"; };
    const char * getMD5(){ return "f388fe853331ce258bf92c0538be78af"; };

  };

}
#endif
