#ifndef _ROS_rees_mDiag_POWER_StopMode_h
#define _ROS_rees_mDiag_POWER_StopMode_h

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "ros/msg.h"
#include "rees/t_POWER_StopMode.h"

namespace rees
{

  class mDiag_POWER_StopMode : public ros::Msg
  {
    public:
      typedef bool _diag_en_type;
      _diag_en_type diag_en;
      typedef rees::t_POWER_StopMode _diag_type;
      _diag_type diag;

    mDiag_POWER_StopMode():
      diag_en(0),
      diag()
    {
    }

    virtual int serialize(unsigned char *outbuffer) const
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.real = this->diag_en;
      *(outbuffer + offset + 0) = (u_diag_en.base >> (8 * 0)) & 0xFF;
      offset += sizeof(this->diag_en);
      offset += this->diag.serialize(outbuffer + offset);
      return offset;
    }

    virtual int deserialize(unsigned char *inbuffer)
    {
      int offset = 0;
      union {
        bool real;
        uint8_t base;
      } u_diag_en;
      u_diag_en.base = 0;
      u_diag_en.base |= ((uint8_t) (*(inbuffer + offset + 0))) << (8 * 0);
      this->diag_en = u_diag_en.real;
      offset += sizeof(this->diag_en);
      offset += this->diag.deserialize(inbuffer + offset);
     return offset;
    }

    const char * getType(){ return "rees/mDiag_POWER_StopMode"; };
    const char * getMD5(){ return "dec5f12816c0e5206b5e3c2662613947"; };

  };

}
#endif
