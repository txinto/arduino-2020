#include <prj_cfg.h>
#include <ROS/prj_ros.h>
#include <KEYPMode/KEYPMode.h>
#include <prj_DRE.h>
#include <DRE.h>

t_prj_diag pdiag;
t_prj_DRE pdre;
extern t_diag diag;
extern t_dre dre;

/***** FSM tasks *****/
void fsmTasksInitBeforeCore(void) {
#ifdef CFG_USE_UNO_LCD1602_KEYPAD
    KEYPModeInit();
#endif
}
void fsmTasksInitAfterCore(void) {
}
void fsmTasksBeforeCore(void) {
#ifdef CFG_USE_UNO_LCD1602_KEYPAD
    KEYPMode();
#endif
}
void fsmTasksAfterCore(void) {
#ifdef CFG_DBG_SERVOALARM_LED
    diag.enable_statusLED = true;
    diag.statusLED = dre.ServoAlarm;
#endif
}
void prj_main_init(){
    fsmTasksInitBeforeCore();
    rees_core_init();
    fsmTasksInitAfterCore();
#ifdef CFG_USE_ROSSERIAL
    prj_ros_setup();
#endif    
}

void prj_main(){
    fsmTasksBeforeCore();
    rees_core();
    fsmTasksAfterCore();    
#ifdef CFG_USE_ROSSERIAL
    prj_ros_loop();
#endif
}