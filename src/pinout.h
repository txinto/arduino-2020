/* ---- This file describes all the pin assignments of the microcontroller --- */

#ifndef _PRJ_PINOUT_H
#define _PRJ_PINOUT_H

#include <Arduino.h>
#include <prj_cfg.h>

#ifdef ARDUINO_ESP8266_WEMOS_D1MINI
//#error "TODO: Definir pines para el ESP8266_WEMOS_D1MINI"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_ESP8266_ESP01
//#error "TODO: Definir pines para el ARDUINO_ESP8266_ESP01"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_ESP8266_NODEMCU
//#error "TODO: Definir pines para el ARDUINO_ESP8266_NODEMCU"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_ESP32_DEV
//#error "TODO: Definir pines para la ARDUINO_AVR_NANO"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_TEENSY31
//#error "TODO: Definir pines para la TEENSY31"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_TEENSY35
//#error "TODO: Definir pines para la TEENSY31"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_AVR_NANO
//#error "TODO: Definir pines para la ARDUINO_AVR_NANO"
#define PIN_statusLED LED_BUILTIN

#elif ARDUINO_AVR_MEGA2560
//#error "TODO: Definir pines para la ARDUINO_AVR_MEGA2560"

#elif ARDUINO_AVR_UNO
//#error "TODO: Definir pines para la ARDUINO_AVR_MEGA2560"

#else
#error "No microcontroller defined"
#endif

#endif // _PRJ_PINOUT_H
