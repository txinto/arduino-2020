#include <prj_cfg.h>
#include <KEYPMode/KEYPMode.h>

typedef struct {
#ifdef CFG_USE_UNO_LCD1602_KEYPAD
#include "KEYPMode/KEYPMode_DRE.h"
#endif
}t_prj_DRE;

typedef struct {
#ifdef CFG_USE_UNO_LCD1602_KEYPAD
#include "KEYPMode/KEYPMode_DRE_diag.h"
#endif
#ifdef CFG_USE_ROSSERIAL    
    uint8_t debug_ros;
#endif
}t_prj_diag;