/* This header file configures the main parameters of the application.
If variant exists, the specific parameters shall be defined
in prj_variants.h file, not here.  The active variant is also selected
in prj_variants.h file.
*/
#ifndef _PRJ_CFG_H
#define _PRJ_CFG_H

/* Include variant specific configuration and active variant selection */
#include <prj_variants.h>

////////  Configuration of functionalities ///////////

//////// Debug Configuration ///////////
/**
 * Set here the debugging directives to enable/disable the debug features of
 * different application features */
//#define DEBUG_CYCLE_TIME
//#define DEBUG_BUT
#define CFG_DBG_SERVOALARM_LED

//#define CFG_USE_ROSSERIAL
//#define CFG_USE_UNO_LCD1602_KEYPAD

/**
 * Microcontroller/platform related layer switch 
 * Some features or layers can not be implemented in some platforms
 * When platformio compiles to an specific platform, sets the platform
 * directive (the information of the directives set is shown in platformi.ini file)
 * Here you can perform specific configuration of the project based on the target platform
 * Typical specific configuration examples are:
 * * unset a layer or feature 
 * ** (Arduino mega does not have wifi support, don't compile IOT, MQTT or WIFI)
 * * reconfigure a parameter
 * ** (Some microcontroller have little RAM, so the size of the buffers can be reduced here
 * ** or some resources hungry features disabled)
 * We include here an example with the hypothetical wifi and mqtt features
 * NOTE: DO NOT USE PLATFORM WHEN YOU NEED TO CONFIGURE VARIANTS.  USE THEM TO REFLECT
 * HOW TO REWRITE THE CODE TO ADAPT IT TO SPECIFIC PLATFORMS, DO NOT BASE YOUR VARIANT 
 * SELECTION OR CONFIGURATION ON THE TARGET THAT IS BEING COMPILED
 */
#ifdef ARDUINO_ESP8266_WEMOS_D1MINI

#elif ARDUINO_ESP8266_ESP01

#elif ARDUINO_ESP8266_NODEMCU

#elif DARDUINO_ESP8266_ESP01

#elif ARDUINO_ESP32_DEV

#elif ARDUINO_TEENSY31
#undef CFG_USE_MQTT
#undef CFG_USE_WIFI

#elif ARDUINO_TEENSY35
#undef CFG_USE_MQTT
#undef CFG_USE_WIFI

#elif ARDUINO_AVR_NANO
#undef CFG_USE_MQTT
#undef CFG_USE_WIFI

#elif ARDUINO_AVR_MEGA2560
#undef CFG_USE_MQTT
#undef CFG_USE_WIFI

#elif ARDUINO_AVR_UNO
#undef CFG_USE_MQTT
#undef CFG_USE_WIFI
#undef CFG_USE_LCD
#else
#error "No microcontroller defined"
#endif

#endif /* _PRJ_CFG_H */
