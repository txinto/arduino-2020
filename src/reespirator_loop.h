
#ifndef _REESPIRATOR_LOOP_HPP
#define _REESPIRATOR_LOOP_HPP

#include <LoopTicker.hpp>
#include <prj_cfg.h>
#include <prj_main.h>

// forward functions
void reset_watchdog(const void* object_ptr, LoopTicker* loop_ticker);
void main_loop(const void* object_ptr, LoopTicker* loop_ticker);


// LoopTicker tasks
static const LoopTicker::TaskEntryPoint loop_tasks[] =
{
    // add here the task entry-points to execute in the main loop
    { .function = reset_watchdog, .object_ptr = nullptr },
    { .function = main_loop, .object_ptr = nullptr },
};

#define LOOP_TASKS_COUNT (sizeof(loop_tasks) / sizeof(loop_tasks[0]))


// reset the hardware watchdog
void reset_watchdog(const void* object_ptr, LoopTicker* loop_ticker)
{
    // ToDo
}

// reset the hardware watchdog
void main_loop(const void* object_ptr, LoopTicker* loop_ticker)
{
    static uint32_t _next_toggle = 0;
    uint32_t now = loop_ticker->getLoopMs32();
    if (now >= _next_toggle)
    {
        prj_main();
        // 5 msecs cycle
        _next_toggle = now + CTE_CYCLE_TIME_IN_MS;
    }    
}

#endif // _REESPIRATOR_LOOP_HPP
