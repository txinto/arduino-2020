/**
 * This header file allows to compile the same project with different
 * variants or flavours.  This file will drive the prj_cfg.h parameters
 * setting specific configuration for each variant
 */ 

#ifndef _PRJ_VARIANTS_H
#define _PRJ_VARIANTS_H

#include <rees_core.h>

#endif /* _PRJ_VARIANTS_H */
